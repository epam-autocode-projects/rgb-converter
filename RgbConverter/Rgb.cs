﻿using System;
using System.Linq;
using System.Text;

namespace RgbConverter
{
    public static class Rgb
    {
        /// <summary>
        /// Gets hexadecimal representation source RGB decimal values.
        /// </summary>
        /// <param name="red">The valid decimal value for RGB is in the range 0-255.</param>
        /// <param name="green">The valid decimal value for RGB is in the range 0-255..</param>
        /// <param name="blue">The valid decimal value for RGB is in the range 0-255k.</param>
        /// <returns>Returns hexadecimal representation source RGB decimal values.</returns>
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            StringBuilder rez = new StringBuilder();
            for (int i = 0; i < 3; i++)
            {
                int n = 0;
                switch (i)
                {
                    case 0: n = red;
                        break;
                    case 1: n = green;
                        break;
                    case 2: n = blue;
                        break;
                }

                if (n <= 0)
                {
                    rez.Append("00");
                }
                else if (n >= 255)
                {
                    rez.Append("FF");
                }
                else
                {
                    rez.Append(n.ToString("X2", null));
                }
            }

            return rez.ToString();
        }
    }
}
